/*
 * Copyright (C) Pietro Pilolli 2010 <pilolli.pietro@gmail.com>
 * 
 * Curtain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Curtain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* Grab pointer. */
void
grab_pointer       (GtkWidget    *win,
                    GdkEventMask  eventmask);


/* Ungrab pointer. */
void
ungrab_pointer     (GdkDisplay  *display);


/* Set the cairo surface color to the RGBA string. */
void
cairo_set_source_color_from_string     (cairo_t  *cr,
                                        gchar    *color);


/* Scale the surface with the width and height requested */
cairo_surface_t *
scale_surface      (cairo_surface_t  *surface,
                    gdouble           width,
                    gdouble           height);



/*
 * Copyright (C) Pietro Pilolli 2010 <pilolli.pietro@gmail.com>
 * 
 * Curtain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Curtain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include <cairo.h>

#ifdef _WIN32
#define CURTAIN_FILE "..\\share\\curtain\\ui\\icons\\curtain.png"
#else
#define CURTAIN_FILE PACKAGE_DATA_DIR"/curtain/ui/icons/curtain.png"
#endif 
/* This is for local testing*/
//#define CURTAIN_FILE "curtain.png"

#ifdef _WIN32
/* User for grab the pointer on win32. */
#define CURTAIN_MOUSE_EVENTS    (GDK_POINTER_MOTION_MASK |   \
                                 GDK_BUTTON_PRESS_MASK   |   \
                                 GDK_BUTTON_RELEASE_MASK |   \
                                 GDK_PROXIMITY_IN        |   \
                                 GDK_PROXIMITY_OUT       |   \
                                 GDK_MOTION_NOTIFY       |   \
                                 GDK_BUTTON_PRESS   \
                                )
#endif			
	

/* Destroy window. */		
gboolean
destroy            (GtkWidget  *widget,
                    gpointer    data);


/* On screen changed. */
void on_window_screen_changed       (GtkWidget  *widget,
                                     GdkScreen  *previous_screen,
                                     gpointer    user_data);



/*
 * Copyright (C) Pietro Pilolli 2010 <pilolli.pietro@gmail.com>
 * 
 * Curtain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Curtain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "curtain.h"
#include "callbacks.h"
#include "utils.h"


#ifndef _WIN32
/* Call the dialog that inform the user to enable a composite manager. */
static void
run_missing_composite_manager_dialog   ()
{
  GtkWidget *msg_dialog;
  msg_dialog = gtk_message_dialog_new (NULL,
                                       GTK_DIALOG_MODAL,
                                       GTK_MESSAGE_ERROR,
                                       GTK_BUTTONS_OK,
                                       gettext ("In order to run Curtain you need to enable a composite manager"));

  gtk_dialog_run (GTK_DIALOG (msg_dialog));

  if (msg_dialog != NULL)
    {
      gtk_widget_destroy (msg_dialog);
      msg_dialog = NULL;
    }

  exit (EXIT_FAILURE);
}


/* Check if a composite manager is active. */
static void
check_composite_manager      ()
{
  GdkDisplay *display = gdk_display_get_default ();
  GdkScreen  *screen  = gdk_display_get_default_screen (display);
  gboolean composite = gdk_screen_is_composited (screen);

  if (!composite)
    {
      /* start the enable composite manager dialog. */
      run_missing_composite_manager_dialog ();
    }

}

#endif


/* Enable the localization support with gettext. */
static void
enable_localization_support       ()
{
#ifdef ENABLE_NLS
  setlocale (LC_ALL, "");
  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (GETTEXT_PACKAGE);
#endif
}


/* Create the window. */
GtkWidget*
create_window      (void)
{
  GtkWidget *window = (GtkWidget *) NULL;
  GtkBuilder *builder = (GtkBuilder *) NULL;
  GError* error = (GError *) NULL;

  builder = gtk_builder_new ();
  if (!gtk_builder_add_from_file (builder, UI_FILE, &error))
    {
      g_warning ("Couldn't load builder file: %s", error->message);
      g_error_free (error);
    }

  window = GTK_WIDGET (gtk_builder_get_object (builder, "window"));

  /* This trys to set an alpha channel. */
  on_window_screen_changed(window, NULL, NULL);
  gint width = gdk_screen_width ();
  gint height = gdk_screen_height ();

  /* In the gtk 2.16.6 the gtkbuilder property double-buffered is not parsed from the glade file
   * and then I set this by hands.
   */
  gtk_widget_set_double_buffered (window, FALSE);
  gtk_widget_set_size_request (window, width, height);
  gtk_window_fullscreen (GTK_WINDOW (window));

  /* This is important; connect the gtk signals for callbacks. */
  gtk_builder_connect_signals (builder, NULL);

  g_object_unref (builder);

  return window;
}


/* Main entry of the program. */
int
main          (int    argc,
               char  *argv[])
{
  GtkWidget *window;

  /* Enable the localization support with gettext. */
  enable_localization_support ();
  gtk_init (&argc, &argv);

#ifndef _WIN32
  check_composite_manager ();
#endif

  window = create_window ();
  gtk_window_set_keep_above(GTK_WINDOW(window), TRUE);
  gtk_widget_show_all (window);

  gtk_main ();
  return 0;
}


